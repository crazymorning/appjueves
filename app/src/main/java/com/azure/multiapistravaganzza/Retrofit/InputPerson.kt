package com.azure.multiapistravaganzza.Retrofit

data class InputPerson(
    val gender: String,
    val name: String,
    val region: String,
    val surname: String
)