package com.azure.multiapistravaganzza.Retrofit

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    //  @GET("genre/movie/list")
    // fun getGenre(@Query("api_key") api_key : String = RetrofitConfig.apiKey, @Query("language") language: String = "es-ES"): Call<>

    @GET("api")
    fun getYesNo(): Call<InputYesNo>
    @GET("api")
    fun getPerson(): Call<InputPerson>
    @GET("v2/list")
    fun getImage(): Call<List<InputPics>>
    @GET("goatse/200/200")
    fun getGoat(): Call<String>
}