package com.azure.multiapistravaganzza.Retrofit

import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.Serializable

object RetroConfig {

    lateinit var service: ApiService
    val TAG = "TagTest"
    val urlYesNo = "https://yesno.wtf/"
    val urlPersonGenerator = "https://uinames.com/"
    var urlPics = "https://picsum.photos/"
    var urlGoat = "https://placegoat.com/"

    fun initService(url: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        service = retrofit.create(ApiService::class.java)
    }


}