package com.azure.multiapistravaganzza.Retrofit

data class InputPics(
    val author: String,
    val download_url: String,
    val height: Int,
    val id: String,
    val url: String,
    val width: Int
)
