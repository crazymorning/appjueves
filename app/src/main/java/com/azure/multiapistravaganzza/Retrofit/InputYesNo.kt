package com.azure.multiapistravaganzza.Retrofit

import java.io.Serializable

data class InputYesNo(
    val answer: String?,
    val forced: Boolean?,
    val image: String?
):Serializable