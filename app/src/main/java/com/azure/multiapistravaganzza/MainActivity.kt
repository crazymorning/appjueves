package com.azure.multiapistravaganzza

import android.app.Person
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.azure.multiapistravaganzza.Fragmentes.FragmentPic
import com.azure.multiapistravaganzza.Fragmentes.FragmentYesNo
import com.azure.multiapistravaganzza.Fragmentes.GoatFragment
import com.azure.multiapistravaganzza.Fragmentes.PersonGenerator
import com.azure.multiapistravaganzza.Navigation.Navigator
import com.azure.multiapistravaganzza.Retrofit.RetroConfig.initService
import com.azure.multiapistravaganzza.Retrofit.RetroConfig.urlYesNo
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Navigator.ChangeFragment(this, FragmentYesNo(), "FragmentYesNo", false)
        initService(urlYesNo)
        bottom_navigation_view.setOnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.action_music -> {
                    Navigator.ChangeFragment(this, FragmentYesNo(), "FragmentYesNo", true)
                    true
                }
                R.id.action_films -> {
                    Navigator.ChangeFragment(this, PersonGenerator(), "PersonGenerator", true)
                    true
                }
                R.id.action_books -> {
                    Navigator.ChangeFragment(this, FragmentPic(), "FragmentPic", true)
                    true
                }
                R.id.action_goat -> {
                    Navigator.ChangeFragment(this, GoatFragment(), "GoatFragment", true)
                    true
                }
                else -> false
            }
        }
    }

    }





