package com.azure.multiapistravaganzza.Fragmentes

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.azure.multiapistravaganzza.R
import com.azure.multiapistravaganzza.Retrofit.InputYesNo
import com.azure.multiapistravaganzza.Retrofit.RetroConfig
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_fragment_yes_no.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class FragmentYesNo : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getYesNo(this)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_yes_no, container, false)
    }

    fun getYesNo(fragment: Fragment) {
        val call = RetroConfig.service.getYesNo()
        call.enqueue(object : Callback<InputYesNo> {
            override fun onFailure(call: Call<InputYesNo>, t: Throwable) {
                Log.v(RetroConfig.TAG, "Something went wrong connecting")
            }

            override fun onResponse(call: Call<InputYesNo>, response: Response<InputYesNo>) {
                var body = response.body()
                if(response.isSuccessful && body != null){
                    Log.v(RetroConfig.TAG, "connection succesfull")
                    answer.text = body.answer
                    Glide.with(fragment).load(body.image).into(gif)
                    Log.d("output", body.toString())


                }
            }

        })
    }
}
