package com.azure.multiapistravaganzza.Fragmentes

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.azure.multiapistravaganzza.R
import com.azure.multiapistravaganzza.RecyclerStuff.MyAdapter
import com.azure.multiapistravaganzza.Retrofit.InputPics
import com.azure.multiapistravaganzza.Retrofit.InputYesNo

import com.azure.multiapistravaganzza.Retrofit.RetroConfig
import com.azure.multiapistravaganzza.Retrofit.RetroConfig.initService
import com.azure.multiapistravaganzza.Retrofit.RetroConfig.urlPics
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_fragment_pic.*
import kotlinx.android.synthetic.main.fragment_fragment_yes_no.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class FragmentPic : Fragment() {

lateinit var recicler: RecyclerView
    lateinit var adapter: MyAdapter
    var data = ArrayList<InputPics>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_pic, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initService(urlPics)
        getPics(this)
        initAdapter()
    }
    fun initAdapter() {
        recicler = fragmentPic
        val mLayoutManager = GridLayoutManager(context, 2)
        recicler.layoutManager = mLayoutManager

        adapter = MyAdapter(data)
        recicler.adapter = adapter
    }


    fun getPics(fragment: Fragment) {
        val call = RetroConfig.service.getImage()
        call.enqueue(object : Callback<List<InputPics>> {
            override fun onFailure(call: Call<List<InputPics>>, t: Throwable) {
                Log.v("output", "Something went wrong connecting")
            }

            override fun onResponse(call: Call<List<InputPics>>, response: Response<List<InputPics>>) {
                var body = response.body()
                if(response.isSuccessful && body != null){
                    Log.v("connection", "connection succesfull")
                    data.clear()
                    data.addAll(body)
                    Log.d("output", body.toString())
                    adapter.notifyDataSetChanged()


                }
            }

        })
    }


}