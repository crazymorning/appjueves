package com.azure.multiapistravaganzza.Fragmentes

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.azure.multiapistravaganzza.R
import com.azure.multiapistravaganzza.Retrofit.InputYesNo
import com.azure.multiapistravaganzza.Retrofit.RetroConfig
import com.azure.multiapistravaganzza.Retrofit.RetroConfig.initService
import com.azure.multiapistravaganzza.Retrofit.RetroConfig.urlGoat
import com.bumptech.glide.Glide
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_fragment_yes_no.*
import kotlinx.android.synthetic.main.fragment_goat.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GoatFragment : Fragment() {
    lateinit var goat : String

    override fun onCreate(savedInstanceState: Bundle?) {
        initService(urlGoat)
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_goat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initService(urlGoat)
        giveTheGoat.setOnClickListener {
            getTheGoat(this)}


    }

    fun getGoat(fragment: Fragment) {
        val call = RetroConfig.service.getGoat()
        call.enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.v("output", "Something went wrong connecting goat")
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                var body = response.body()
                if(response.isSuccessful && body != null){
                    Log.v("output", "connection succesfull goat")

                    Glide.with(fragment).load(body).into(Goatse)
                    Log.d("output", body)
                }
            }

        })
    }
    fun getTheGoat(fragment: Fragment){
        val call = RetroConfig.service.getGoat()
        call.enqueue(object : Callback<String>{
            override fun onFailure(call: Call<String>, t: Throwable) {

                Log.v("output", "Something went wrong GOAT")
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                Log.v("output", "Goat connected SUCCESFULLY")
                var body = response.body()
                Log.v("output", body)

            }

        })
    }

}
