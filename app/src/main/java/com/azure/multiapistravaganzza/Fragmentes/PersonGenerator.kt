package com.azure.multiapistravaganzza.Fragmentes

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.azure.multiapistravaganzza.R
import com.azure.multiapistravaganzza.Retrofit.InputPerson
import com.azure.multiapistravaganzza.Retrofit.InputYesNo
import com.azure.multiapistravaganzza.Retrofit.RetroConfig
import com.azure.multiapistravaganzza.Retrofit.RetroConfig.initService
import com.azure.multiapistravaganzza.Retrofit.RetroConfig.urlPersonGenerator
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_fragment_yes_no.*
import kotlinx.android.synthetic.main.fragment_person_generator.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class PersonGenerator : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initService(urlPersonGenerator)
        getPerson(this)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_person_generator, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        generatePerson.setOnClickListener {
            getPerson(this)
        }
    }
    fun getPerson(fragment: Fragment) {
        val call = RetroConfig.service.getPerson()
        call.enqueue(object : Callback<InputPerson> {
            override fun onFailure(call: Call<InputPerson>, t: Throwable) {
                Log.v(RetroConfig.TAG, "Something went wrong connecting")
            }

            override fun onResponse(call: Call<InputPerson>, response: Response<InputPerson>) {
                var body = response.body()
                if(response.isSuccessful && body != null){
                    Log.v(RetroConfig.TAG, "connection succesfull")
                    name.text = body.name
                    gender.text = body.gender
                    surname.text = body.surname
                    region.text = body.region
                    Log.d("output", body.toString())



                }
            }

        })
    }
}