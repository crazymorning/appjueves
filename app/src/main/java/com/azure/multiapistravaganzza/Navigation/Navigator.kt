package com.azure.multiapistravaganzza.Navigation

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.Fragment
import com.azure.multiapistravaganzza.R

object Navigator{



    fun ChangeFragment(activity: AppCompatActivity?, fragment: Fragment, name: String, backStack: Boolean = true ){
        val navigate = activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.mainFrame, fragment, "redFragment")
        if(backStack){
            navigate?.addToBackStack(name)?.commit()
        }else{
            navigate?.commit()
        }
    }

}