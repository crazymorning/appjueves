package com.azure.multiapistravaganzza.RecyclerStuff

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.azure.multiapistravaganzza.R
import com.azure.multiapistravaganzza.Retrofit.InputPics
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_pic.view.*

class MyAdapter(private val dame: ArrayList<InputPics>) : RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pic, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(dame[position])
    }

    override fun getItemCount(): Int {
        return dame.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("ResourceAsColor")
        fun bind(item: InputPics) {
            Picasso.get().load("${item.download_url}.jpg").into(itemView.pic)
            itemView.author.text = item.author

        }
    }
}
